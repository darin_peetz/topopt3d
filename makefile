ifeq ($(DEBUG),yes)
    OPT_FLAG = -g3
    PETSC_ARCH = arch-linux-debug
else
    OPT_FLAG = -g0 -O3
    PETSC_ARCH = arch-linux-opt
endif

include ${SLEPC_DIR}/lib/slepc/conf/slepc_common

COMPILE = mpicxx -fPIC -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas ${OPT_FLAG} \
          -I${MYLIB_DIR}/Eigen -I${MYLIB_DIR}/parmetis-4.0.3/include -I${SLEPC_DIR}/include \
          -I${SLEPC_DIR}/${PETSC_ARCH}/include -I${PETSC_DIR}/include \
          -I${PETSC_DIR}/${PETSC_ARCH}/include

LINK  =   mpicxx -fPIC -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas ${OPT_FLAG} -L${MYLIB_DIR}/parmetis-4.0.3/lib

all: TopOpt

tidy:
	rm -f *.o

TopOpt: Main.o TopOpt.o Inputs.o RecMesh.o Filter.o MMA.o FEAnalysis.o Functions.o Volume.o Compliance.o Perimeter.o Buckling.o Dynamic.o EigenPeetz.o
	${LINK} Main.o TopOpt.o Inputs.o RecMesh.o Filter.o MMA.o EigenPeetz.o Functions.o FEAnalysis.o Volume.o Compliance.o Perimeter.o Buckling.o Dynamic.o -o TopOpt ${SLEPC_EPS_LIB} -lparmetis -lmetis

TopOpt.o: TopOpt.cpp TopOpt.h
	${COMPILE} ${SLEPC_EPS_LIB} TopOpt.cpp -c -o TopOpt.o

Main.o: Main.cpp Inputs.h TopOpt.h MMA.h Functions.h
	${COMPILE} ${SLEPC_EPS_LIB} Main.cpp -c -o Main.o

Inputs.o: Inputs.cpp Inputs.h TopOpt.h MMA.h EigLab.h
	${COMPILE} ${PETSC_KSP_LIB} Inputs.cpp -c -o Inputs.o

RecMesh.o: RecMesh.cpp RecMesh.h TopOpt.h Inputs.h EigLab.h
	${COMPILE} ${PETSC_KSP_LIB} RecMesh.cpp -c -o RecMesh.o

Filter.o: Filter.cpp TopOpt.h
	${COMPILE} ${PETSC_KSP_LIB} Filter.cpp -c -o Filter.o

MMA.o: MMA.cpp MMA.h
	${COMPILE} ${PETSC_KSP_LIB} MMA.cpp -c -o MMA.o

FEAnalysis.o: FEAnalysis.cpp TopOpt.h
	${COMPILE} ${PETSC_KSP_LIB} FEAnalysis.cpp -c -o FEAnalysis.o

Functions.o: Functions.cpp Functions.h TopOpt.h
	${COMPILE} ${SLEPC_EPS_LIB} Functions.cpp -c -o Functions.o

Volume.o: Volume.cpp Functions.h TopOpt.h
	${COMPILE} ${PETSC_KSP_LIB} Volume.cpp -c -o Volume.o

Compliance.o: Compliance.cpp Functions.h TopOpt.h
	${COMPILE} ${PETSC_KSP_LIB} Compliance.cpp -c -o Compliance.o

Perimeter.o: Perimeter.cpp Functions.h TopOpt.h
	${COMPILE} ${PETSC_KSP_LIB} Perimeter.cpp -c -o Perimeter.o

Buckling.o: Buckling.cpp Functions.h TopOpt.h EigenPeetz.h
	${COMPILE} ${SLEPC_EPS_LIB} Buckling.cpp -c -o Buckling.o

Dynamic.o: Dynamic.cpp Functions.h TopOpt.h EigenPeetz.h
	${COMPILE} ${SLEPC_EPS_LIB} Dynamic.cpp -c -o Dynamic.o

EigenPeetz.o: EigenPeetz.cpp EigenPeetz.h
	${COMPILE} ${SLEPC_EPS_LIB} EigenPeetz.cpp -c -o EigenPeetz.o
